# Vezeeta Website

## Description
This project is a web application built with C# .NET Core, Entity Framework Core, and ASP.NET Web API for managing doctors, patients, requests, and settings in a medical context.

## Features
- CRUD operations for Doctors, Patients, Requests, and Settings
- Dashboard to view statistics (number of doctors, patients, requests, top specializations, etc.)
- Search functionalities for doctors and patients
- Manage discount code coupons
- Authentication and authorization using IdentityServer4 and JWT
- Support for localization in Arabic (ar) and English (en)
- Social login functionality

## Technical Stack
- C# .NET Core
- Entity Framework Core
- ASP.NET Web API
- Swagger for API documentation/testing
- IdentityServer4 for authentication and authorization
- Localization for multi-language support
- Social login integration
- JWT Authentication for secure access
