using Microsoft.AspNetCore.Mvc;
using Vzeeta.Models;
using Vzeeta.Repositories;

namespace Vzeeta.Controllers
{
    [ApiController]
    [Route("api/settings")]
    public class SettingsController : ControllerBase
    {
        private readonly ISettingRepository _settingRepository;

        public SettingsController(ISettingRepository settingRepository)
        {
            _settingRepository = settingRepository;
        }

        [HttpPost("add")]
        public IActionResult AddDiscountCode([FromBody] DiscountCode discountCode)
        {
            bool added = _settingRepository.AddDiscountCode(discountCode);
            if (added)
            {
                return Ok(true);
            }
            return BadRequest("Failed to add the discount code.");
        }

        [HttpPut("update")]
        public IActionResult UpdateDiscountCode([FromBody] DiscountCode discountCode)
        {
            bool updated = _settingRepository.UpdateDiscountCode(discountCode);
            if (updated)
            {
                return Ok(true);
            }
            return BadRequest("Failed to update the discount code.");
        }

        [HttpDelete("delete/{id}")]
        public IActionResult DeleteDiscountCode(int id)
        {
            bool deleted = _settingRepository.DeleteDiscountCode(id);
            if (deleted)
            {
                return Ok(true);
            }
            return BadRequest("Failed to delete the discount code.");
        }

        [HttpPut("deactivate/{id}")]
        public IActionResult DeactivateDiscountCode(int id)
        {
            bool deactivated = _settingRepository.DeactivateDiscountCode(id);
            if (deactivated)
            {
                return Ok(true);
            }
            return BadRequest("Failed to deactivate the discount code.");
        }
    }
}
