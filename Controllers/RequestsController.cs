using Microsoft.AspNetCore.Mvc;
using Vzeeta.Models;
using Vzeeta.Repositories;

namespace Vzeeta.Controllers
{
    [ApiController]
    [Route("api/requests")]
    public class RequestsController : ControllerBase
    {
        private readonly IRequestRepository _requestRepository;

        public RequestsController(IRequestRepository requestRepository)
        {
            _requestRepository = requestRepository;
        }

        [HttpGet]
        public IActionResult GetAllRequests()
        {
            var requests = _requestRepository.GetAllRequests();
            return Ok(requests);
        }

        [HttpGet("{id}")]
        public IActionResult GetRequestById(int id)
        {
            var request = _requestRepository.GetRequestById(id);
            if (request == null)
            {
                return NotFound();
            }
            return Ok(request);
        }

        [HttpPost]
        public IActionResult AddRequest([FromBody] Request request)
        {
            _requestRepository.AddRequest(request);
            return CreatedAtAction(nameof(GetRequestById), new { id = request.Id }, request);
        }

    }
}
