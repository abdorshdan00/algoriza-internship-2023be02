using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vzeeta.Models;
using Vzeeta.Repositories;

namespace Vzeeta.Controllers
{
    [ApiController]
    [Route("api/patients")]
    public class PatientsController : ControllerBase
    {
        private readonly IPatientRepository _patientRepository;
        private readonly IRequestRepository _requestRepository; // Assuming a request repository

        public PatientsController(IPatientRepository patientRepository, IRequestRepository requestRepository)
        {
            _patientRepository = patientRepository;
            _requestRepository = requestRepository;
        }

        [HttpGet]
        public IActionResult GetAllPatients(int page = 1, int pageSize = 10, string search = "")
        {
            var patients = _patientRepository.GetAllPatients(page, pageSize, search);
            return Ok(patients);
        }

        [HttpGet("{id}")]
        public IActionResult GetPatientById(int id)
        {
            var patient = _patientRepository.GetPatientById(id);
            if (patient == null)
            {
                return NotFound();
            }

            // Fetch requests associated with the patient
            var requests = _requestRepository.GetRequestsForPatient(id);

            var patientDetails = new
            {
                // Details of the patient
                details = patient,
                requests = requests
            };

            return Ok(patientDetails);
        }
    }
}
