using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Vzeeta.Models;
using Vzeeta.Repositories;
using Vzeeta.Services;

namespace Vzeeta.Controllers
{
    [ApiController]
    [Route("api/doctors")]
    public class DoctorsController : ControllerBase
    {
        private readonly IDoctorRepository _doctorRepository;
        private readonly IFeedbackRepository _feedbackRepository; // Assuming a feedback repository

        public DoctorsController(IDoctorRepository doctorRepository, IFeedbackRepository feedbackRepository)
        {
            _doctorRepository = doctorRepository;
            _feedbackRepository = feedbackRepository;
        }

        [HttpGet]
        public IActionResult GetAllDoctors(int page = 1, int pageSize = 10, string search = "")
        {
            var doctors = _doctorRepository.GetAllDoctors(page, pageSize, search);
            return Ok(doctors);
        }

        [HttpGet("{id}")]
        public IActionResult GetDoctorById(int id)
        {
            var doctor = _doctorRepository.GetDoctorById(id);
            if (doctor == null)
            {
                return NotFound();
            }

            var feedbacks = _feedbackRepository.GetFeedbacksForDoctor(id);

            var doctorDetails = new
            {
                details = doctor,
                feedbacks = feedbacks
            };

            return Ok(doctorDetails);
        }

        [HttpPost]
        public IActionResult AddDoctor([FromBody] Doctor doctor)
        {
            _doctorRepository.AddDoctor(doctor);
            return CreatedAtAction(nameof(GetDoctorById), new { id = doctor.Id }, doctor);
        }

        [HttpPut("{id}")]
        public IActionResult EditDoctor(int id, [FromBody] Doctor doctor)
        {
            var existingDoctor = _doctorRepository.GetDoctorById(id);
            if (existingDoctor == null)
            {
                return NotFound();
            }

            doctor.Id = id; 
            _doctorRepository.UpdateDoctor(doctor);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteDoctor(int id)
        {
            var doctor = _doctorRepository.GetDoctorById(id);
            if (doctor == null)
            {
                return NotFound();
            }

            // Check if the doctor has any requests associated (pseudo code)
            if (!_doctorRepository.HasRequests(id))
            {
                _doctorRepository.DeleteDoctor(id);
                return Ok();
            }

            return BadRequest("Doctor has associated requests. Cannot be deleted.");
        }
    }
}
