using System.Collections.Generic;
using System.Linq;
using Vzeeta.Models;

namespace Vzeeta.Repositories
{
    public class SettingRepository : ISettingRepository
    {
        private readonly YourDbContext _context;

        public SettingRepository(YourDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Setting> GetAllSettings()
        {
            return _context.Settings.ToList();
        }

        public Setting GetSettingById(int id)
        {
            return _context.Settings.FirstOrDefault(s => s.Id == id);
        }

        public void AddSetting(Setting setting)
        {
            _context.Settings.Add(setting);
            _context.SaveChanges();
        }

        public void UpdateSetting(Setting setting)
        {
            _context.Settings.Update(setting);
            _context.SaveChanges();
        }

        public void DeleteSetting(int id)
        {
            var setting = _context.Settings.Find(id);
            if (setting != null)
            {
                _context.Settings.Remove(setting);
                _context.SaveChanges();
            }
        }
        
    }
}
