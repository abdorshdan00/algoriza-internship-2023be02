using System.Collections.Generic;
using System.Linq;
using Vzeeta.Models;

namespace Vzeeta.Repositories
{
    public class RequestRepository : IRequestRepository
    {
        private readonly YourDbContext _context;

        public RequestRepository(YourDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Request> GetAllRequests()
        {
            return _context.Requests.ToList();
        }

        public Request GetRequestById(int id)
        {
            return _context.Requests.FirstOrDefault(r => r.Id == id);
        }

        public void AddRequest(Request request)
        {
            _context.Requests.Add(request);
            _context.SaveChanges();
        }

        public void UpdateRequest(Request request)
        {
            _context.Requests.Update(request);
            _context.SaveChanges();
        }

        public void DeleteRequest(int id)
        {
            var request = _context.Requests.Find(id);
            if (request != null)
            {
                _context.Requests.Remove(request);
                _context.SaveChanges();
            }
        }
        
    }
}
