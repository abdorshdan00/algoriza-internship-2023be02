using System;
using System.Net.Mail;
using Vzeeta.Models;

namespace Vzeeta.Services
{
    public interface IEmailService
    {
        void SendDoctorCredentials(Doctor doctor, string username, string password);
    }

    public class EmailService : IEmailService
    {
        public void SendDoctorCredentials(Doctor doctor, string username, string password)
        {
            try
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(doctor.Email);
                mailMessage.Subject = "Credentials for your account";
                mailMessage.Body = $"Dear Dr. {doctor.LastName},\n\nYour username is: {username}\nYour password is: {password}\n\nRegards,\nThe Administration Team";
                
                var smtpClient = new SmtpClient("smtp.example.com");
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error sending email: " + ex.Message);
            }
        }
    }
}
