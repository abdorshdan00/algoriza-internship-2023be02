namespace Vzeeta.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public string DiscountCode { get; set; }
        public int NumRequestsCompleted { get; set; }

        public DiscountType DiscountType { get; set; }
        public decimal Value { get; set; }
    }

    public enum DiscountType
    {
        Percentage,
        Value
    }
}
