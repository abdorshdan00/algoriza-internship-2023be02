using System;
using System.ComponentModel.DataAnnotations;

namespace Vzeeta.Models
{
    public class Request
    {
        public int Id { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Day { get; set; }
        public string Time { get; set; }
        public decimal Price { get; set; }
        public string DiscountCode { get; set; }
        public decimal FinalPrice { get; set; }

        public RequestStatus Status { get; set; }
    }

    public enum RequestStatus
    {
        Pending,
        Completed,
        Cancelled
        // Add more status options as needed
    }
}
